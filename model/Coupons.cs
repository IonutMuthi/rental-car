﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarRentalApp
{
    class Coupons
    {
        private string code;
        private string description;
        private double discount;

        public Coupons(string code, string description, double discount)
        {
            this.code = code;
            this.description = description;
            this.discount = discount;
        }
    }
}
