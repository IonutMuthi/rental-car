﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarRentalApp.DAO
{
    interface CarInterface
    {
        Car getCarById(int id);
        void insert(Car c);
        void update(Car c, int id);

    }
}
