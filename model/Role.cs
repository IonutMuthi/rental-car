﻿using System.Collections.Generic;

namespace CarRentalApp
{
    internal class Role
    {
        private int roleID;
        private string description;
        private string name;
        private List<Permission> permissions;

        public Role(int roleID, string description, string name, List<Permission> permissions)
        {
            this.roleID = roleID;
            this.description = description;
            this.name = name;
            this.permissions = permissions;
        }
    }
}