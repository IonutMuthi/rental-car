﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarRentalApp
{
    class Car
    {
        private int carID;
        private string plate;
        private string manufacturer;
        private string model;
        private double pricePerDay;

        // we will use this constructor to create the car
        public Car(int carID, string plate, string manufacturer, string model, double pricePerDay)
        {
            this.carID = carID;
            this.plate = plate;
            this.manufacturer = manufacturer;
            this.model = model;
            this.pricePerDay = pricePerDay;
        }
        // we will use this constructor to call the car
        public Car() { }
    }
}
