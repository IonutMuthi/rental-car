﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarRentalApp
{
    class Costumer
    {
        private int costumerID;
        private DateTime birthDate;
        private string name;
        private string location;

        // we will use this constructor to create the costumer
        public Costumer(int costumerID, DateTime birthDate, string name, string location)
        {
            this.costumerID = costumerID;
            this.birthDate = birthDate;
            this.name = name;
            this.location = location;
        }

        // we will use this constructor to call the costumer
        public Costumer() { }
    }
}
