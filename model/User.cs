﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarRentalApp
{
    class User
    {
        private int userID;
        private string plate;
        private bool enabled;
        private Role role;

        // we will use this constructor to create the user
        public User(int userID, string plate, bool enabled, Role role)
        {
            this.userID = userID;
            this.plate = plate;
            this.enabled = enabled;
            this.role = role;
        }
        // we will use this constructor to call the user
        public User() { }
    }
}
