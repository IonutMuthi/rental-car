﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarRentalApp
{
    class Permission
    {
        private int permissionID;
        private string description;
        private string name;

        public Permission(int permissionID, string description, string name)
        {
            this.permissionID = permissionID;
            this.description = description;
            this.name = name;
        }
    }
}
